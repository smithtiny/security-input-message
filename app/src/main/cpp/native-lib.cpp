#include <jni.h>
#include <string>
#include <opencv2/core/core.hpp>
#include"preProcessAndDetection.cpp"


using namespace std;
using namespace cv;

extern "C" {
    JNIEXPORT jdouble JNICALL Java_com_example_qqq_cmake_findInscribedCircleJNI(JNIEnv* env, jobject obj, jlong imgAddr,
                                                                                           jdouble rectTLX, jdouble rectTLY, jdouble rectBRX, jdouble rectBRY,
                                                                                           jdoubleArray incircleX, jdoubleArray incircleY, jlong contourAddr);//一对一映射，声明find函数

    JNIEXPORT jdouble JNICALL Java_com_example_qqq_cmake_HandGesture_findInscribedCircleJNI(JNIEnv* env, jobject obj, jlong imgAddr,
                                                                                           jdouble rectTLX, jdouble rectTLY, jdouble rectBRX, jdouble rectBRY,
                                                                                           jdoubleArray incircleX, jdoubleArray incircleY, jlong contourAddr)//一对一映射，find函数的实现
    {
        Mat& img_cpp  = *(Mat*)imgAddr;

        //vector<Point2f>& contour = *(vector<Point2f> *)contourAddr;
        Mat& contourMat = *(Mat*)contourAddr;
        vector<Point2f> contourVec;
        contourMat.copyTo(contourVec);

        double r = 0;
        double targetX = 0;
        double targetY = 0;

        for (int y = (int)rectTLY; y < (int)rectBRY; y++)
        {
            for (int x = (int)rectTLX; x < (int)rectBRX; x++)
            {
                double curDist = pointPolygonTest(contourVec, Point2f(x, y), true);

                if (curDist > r) {
                    r = curDist;
                    targetX = x;
                    targetY = y;
                }
            }
        }

        jdouble outArrayX[] = {0};
        jdouble outArrayY[] = {0};

        outArrayX[0] = targetX;
        outArrayY[0] = targetY;

        env->SetDoubleArrayRegion(incircleX, 0 , 1, (const jdouble*)outArrayX);
        env->SetDoubleArrayRegion(incircleY, 0 , 1, (const jdouble*)outArrayY);
        //Core.circle(img, inCircle, (int)inCircleRadius, new Scalar(240,240,45,0), 2);


        return r;
    }
JNIEXPORT void JNICALL Java_com_example_qqq_cmake_OpenCVWorker_CreateCascadeClassifier(JNIEnv *env, jobject obj, jstring jCascadeFilePath);

JNIEXPORT void JNICALL Java_com_example_qqq_cmake_OpenCVWorker_CreateCascadeClassifier(
        JNIEnv *env, jobject obj, jstring jCascadeFilePath) {
    const char* jpathstr = env->GetStringUTFChars(jCascadeFilePath, NULL);
    string CascadeFilePath(jpathstr);
    fingertip_cascade.load(CascadeFilePath);
    env->ReleaseStringUTFChars(jCascadeFilePath,jpathstr);
}

JNIEXPORT jintArray JNICALL Java_com_example_qqq_cmake_OpenCVWorker_preProcessAndDetection(JNIEnv *env, jobject obj, jstring fnt, jlong imgAddr, jstring jCascadeFilePath);


JNIEXPORT jintArray JNICALL Java_com_example_qqq_cmake_OpenCVWorker_preProcessAndDetection(
        JNIEnv *env, jobject obj, jstring fnt, jlong imgAddr, jstring jCascadeFilePath) {
    Mat& src = *(Mat*) imgAddr;
    jintArray result;
    jint * resultptr;
    string filename = env->GetStringUTFChars(fnt, NULL);

    try
    {
        result = env->NewIntArray(3);
        resultptr = env->GetIntArrayElements(result, NULL);
        //preprocess
//	    preSkinFilter(src);
//	    Hsv_SkinFilter(src);
//	    YCbCr_SkinFilter(src);
    }
    catch (cv::Exception& e) {
        LOGI("nativeCreateObject caught cv::Exception: %s", e.what());
    } catch (...) {
        LOGD("nativeCreateObject caught unknown exception");
    }

    // rotate 90 degree
    transpose(src, src);
    flip(src, src, 1);

    bool click;
    try
    {
        curtime = clock();
//		LOGD("%u",curtime);
        click = detectAndDisplay(filename, mousedlt, c, ac, src, curtime, lastx, lasty, lastarea, finalft, lasttime, velq, ftpointq, deltaq, areaq);
    }
    catch (cv::Exception& e) {
        LOGI("nativeCreateObject caught cv::Exception: %s", e.what());
    } catch (...) {
        LOGD("nativeCreateObject caught unknown exception");
    }

    if (click)
        resultptr[0] = 1;
    else
        resultptr[0] = 0;
    resultptr[1] = mousedlt.x;
    resultptr[2] = mousedlt.y;

    env->ReleaseIntArrayElements(result, resultptr, NULL);

    return result;
}

JNIEXPORT void JNICALL Java_com_example_qqq_cmake_OpenCVWorker_CreateCascadeClassifier(JNIEnv *env, jobject obj, jstring jCascadeFilePath);
JNIEXPORT jintArray JNICALL Java_com_example_qqq_cmake_OpenCVWorker_preProcessAndDetection(JNIEnv *env, jobject obj, jstring fnt, jlong imgAddr, jstring jCascadeFilePath);
JNIEXPORT void JNICALL Java_com_example_qqq_cmake_OpenCVWorker_preSkinFilter(JNIEnv* env, jobject obj, jlong imgAddr);
}

