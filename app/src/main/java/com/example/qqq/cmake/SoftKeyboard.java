/*
 * Copyright (C) 2008-2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
//package com.seu.android.softkeyboard;
package com.example.qqq.cmake;

//package com.random.android.randomkeyboard;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.os.Process;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.text.method.MetaKeyKeyListener;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.InputMethodSubtype;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Date;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

//import org.opencv.android.CameraBridgeViewBase;

/**
 * Example of writing an input method for a soft keyboard.  This code is
 * focused on simplicity over completeness, so it should in no way be considered
 * to be a complete soft keyboard implementation.  Its purpose is to provide
 * a basic example for how you would get started writing an input method, to
 * be fleshed out as appropriate.
 */
public class SoftKeyboard extends InputMethodService
        implements KeyboardView.OnKeyboardActionListener, OpenCVWorker.ResultCallback,
        SurfaceHolder.Callback, View.OnTouchListener, GestureDetector.OnDoubleTapListener {
    static final boolean DEBUG = false;
    
    /**
     * This boolean indicates the optional example code for performing
     * processing of hard keys in addition to regular text generation
     * from on-screen interaction.  It would be used for input methods that
     * perform language translations (such as converting text entered on 
     * a QWERTY keyboard to Chinese), but may not be used for input methods
     * that are primarily intended to be used for on-screen text entry.
     */
    static final boolean PROCESS_HARD_KEYS = true;

    private InputMethodManager mInputMethodManager;

    private LatinKeyboardView mInputView;
    private CandidateView mCandidateView;
    private CompletionInfo[] mCompletions;
    
    private StringBuilder mComposing = new StringBuilder();
    private boolean mPredictionOn;
    private boolean mCompletionOn;
    private int mLastDisplayWidth;
    private boolean mCapsLock;
    private long mLastShiftTime;
    private long mMetaState;
    
    private LatinKeyboard mSymbolsKeyboard;
    private LatinKeyboard mSymbolsShiftedKeyboard;
    private LatinKeyboard mQwertyKeyboard;
    //lingzhen
    private LatinKeyboard mKeypad;
    private LatinKeyboard mRandomSymbolsKeyboard;
    private LatinKeyboard mRandomSymbolsShiftedKeyboard;
    private LatinKeyboard mRandomQwertyKeyboard;
    
    private LatinKeyboard mCurKeyboard;
    
    private String mWordSeparators;
    
    private static final String TAG = "OCVSample::Activity";
//    private CameraBridgeViewBase   mOpenCvCameraView;
//    private MyCameraView mOpenCvCameraView;
    private View mOpenCvView;
    private View newView;
    private FrameLayout newLinearLayout;
    private FrameLayout m_panel_view;
    private LinearLayout LinearLayout_text;
    public static TextView text_x;
    public static TextView text_y;
    public static TextView text_dx;
    public static TextView text_dy;
    public static TextView text_d;
    public static TextView text_dt;
//    private Mat mRgba;
//    private Mat mRgbaF;
//    private Mat mRgbaT;
    public CursorService m_CurService;
	int m_nScreenW = 0, m_nScreenH = 0;
	boolean m_bRunning = false;
	int inital = 0;
		
	public static Handler tHandler;
	//Mode that presamples hand colors
	public static final int SAMPLE_MODE = 0;
	
	//Mode that generates binary image
	public static final int DETECTION_MODE = 1; 
	
	//Mode that displays color image together with contours, fingertips,
	//defect points and so on.
	public static final int TRAIN_REC_MODE = 2;
	
	//Mode that presamples background colors
	public static final int BACKGROUND_MODE = 3;
	
	//Initial mode is BACKGROUND_MODE to presample the colors of the hand
	private int mode = BACKGROUND_MODE;
	private static final int SAMPLE_NUM = 7;
	
	private int squareLen;
	private Scalar mColorsRGB[] = null;
	private Point[][] samplePoints = null;
	private double[][] avgColor = null;
	private double[][] avgBackColor = null;
    
	//Stores all the information about the hand
	private HandGesture hg = null;
	private Mat rgbaMat = null;
	private Mat interMat = null;
	private Mat rgbMat = null;
	private Mat bgrMat = null;
	private Mat binMat = null;
	private Mat binTmpMat = null;
	private Mat binTmpMat2 = null;
	
	private Mat tmpMat = null;
	
	private Mat[] sampleMats = null;
	
	private double[][] cLower = new double[SAMPLE_NUM][3];
	private double[][] cUpper = new double[SAMPLE_NUM][3];
	private double[][] cBackLower = new double[SAMPLE_NUM][3];
	private double[][] cBackUpper = new double[SAMPLE_NUM][3];
	
	private Scalar lowerBound = new Scalar(0, 0, 0);
	private Scalar upperBound = new Scalar(0, 0, 0);
	
    //Color Space used for hand segmentation
	private static final int COLOR_SPACE = Imgproc.COLOR_RGB2Lab;
	
	public PowerManager pm;
	public PowerManager.WakeLock mWakeLock;
	
	// the size of each key
    // Sabrina
	private static final int KEY_HEIGHT = 60;// 60 dip
	private static final int ROWNUM = 4;
	
    public static final int DRAW_RESULT_BITMAP = 10;
    public static final int DRAW_CURSOR_POINT = 11;
    public static final int RAW_POINT = 12;
    private Handler mSurfaceViewHandler;
    private SurfaceView mSurfaceView;
    private SurfaceHolder mSurfaceHolder;
    private RectF mSurfaceSize;
    private OpenCVWorker mWorker;
    private double mFpsResult;
    private Paint mFpsPaint;
    private GestureDetector mGestureDetector;
    private boolean drawCanvas = true; 
	public Point getPoint = new Point(0,0); 
	
	//Sabrina
	private int[] acc = {16,18,20,22,24,26};
	private int[] thre = {2,3,4,5,6,8};
	private int randomacc = 0;
	private int numerator = 14;
	private int denominator = 2;
	private int threshold = 3;
//	private int numerator = 5;
//	private int denominator = 2;
//	private int threshold = 45;
	
    //Sabrina
	private SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
	private String fName= df.format(new Date());
//	public File xyinfo = new File(Environment.getExternalStorageDirectory(),"xy_mousevalue"+".txt");
//	public File clickinfo = new File(Environment.getExternalStorageDirectory(),"clickdata-"+fName+".txt");
	public File dxyinfo = new File(Environment.getExternalStorageDirectory(),"deltadata"+".txt");
	
   
	
	public boolean inputstate = true;
    public int row = 5;
    
    private String[] keylista = {"q","w","e","r","t","y","u","i","o","p","a","s","d","f","g","h","j","k","l","shift","z","x","c","v","b","n","m","del",
   			"done","123","space","point","next"};
    private String[] keylistb = {"Q","W","E","R","T","Y","U","I","O","P","A","S","D","F","G","H","J","K","L","shift","Z","X","C","V","B","N","M","del",
   			"done","123","space","point","next"};
    private String[] keylistc = {"1","2","3","4","5","6","7","8","9","0","@","#","$","%","&","*","-","=","(",")","shift","!","\"","'",":",";","/","?","del",
   			"done","ABC","space","comma","next"};
    private String[] keylistd = {"~","±","×","÷","•","°","`","´","{","}","©","£","€","^","®","¥","_","+","[","]","shift","¡","&lt;","&gt;","¢","|","\\","¿",
    		"del","done","ABC","space","apostrophe","next"};
    private String[] keylistp = {"7","8","9","4","5","6","1","2","3","del","0","next"};
   	
	private String[][] keyinfoa = new String[keylista.length][row];
    private String[][] keyinfob = new String[keylistb.length][row];
    private String[][] keyinfoc = new String[keylistc.length][row];
    private String[][] keyinfod = new String[keylistd.length][row];
    private String[][] keyinfop = new String[keylistp.length][row];
    private String[][] subkeyinfo;
    
//    private int label = 1;
//	int lastlabel;

    public Point curpoint;
    public Point lastpoint;
    public boolean nacc;
//    public int curx = 0,cury = 0;
    public long init;
    
    public Map<String, Integer> keymap = new HashMap<String, Integer>();
    
    //a h j k del shift A h shift shift A H 123 5 # ! del ABC A 123 " " , shift + [ … shift & ABC A shift shift a h
    public int[][] c = {{100,1500},{600,1600},{800,1550},{900,1500},{950,1700},{100,1700},{100,1500},{600,1600},
    		//a h j k del shift A h
    		{100,1700},{100,1700},{100,1500},{600,1600},{300,1800},{500,1400},{200,1500},{200,1700},{1000,1700},
    		//shift shift A H 123 5 # ! del
    		{300,1800},{100,1500},{300,1800},{500,1900},{800,1800},{100,1700},{800,1600},{900,1500},{800,1900},
    		//ABC A 123 " " , shift + [ …
    		{100,1700},{500,1600},{300,1800},{100,1500},{100,1700},{100,1700},{100,1500},{600,1600}};
     		//shift & ABC A shift shift a h
    public int ccc = 0;
    
    int pCount = 0;
	double d;
	double dy;
	double dx;
	double vy;
	double vx;
	double v;
	double dt;
	long curTime;
	long preTime;

    Deque<RawPoint> queue = new ArrayDeque<RawPoint>();
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == RAW_POINT) {  
//            	Point handpoint = (Point) msg.obj;
            	//Point handpoint = new Point(((double[])msg.obj)[0],((double[])msg.obj)[1]);
                Point handpoint = new Point(((int[])msg.obj)[0],((int[])msg.obj)[1]);
            	int click = (int)((double[])msg.obj)[2];
            	
                String c;
            	if (click == 1)
            		c = "true";
            	else
            		c = "false";
            	
            	try {
    				FileOutputStream fos = new FileOutputStream(dxyinfo, true);
    				fos.write((Integer.toString(handpoint.x)+","+ Integer.toString(handpoint.y)+","+c+"\n").getBytes());
    				fos.close();
    			} catch (Exception e) {
    				e.printStackTrace();
    			}
            	
            	if(tHandler != null){
//            		if(pCount == 0){
//            			curTime = Core.getTickCount();
//            			preTime = curTime;
//            			RawPoint cdata = new RawPoint(handpoint, 0);
//            			queue.add(cdata);
//            			pCount = pCount + 1;
//            		}else{
//            			curTime = Core.getTickCount();
//            			if(queue.size() == 10){
//            				queue.remove();
//            			}
//            			RawPoint pdata = queue.getLast();
//            			RawPoint cdata = new RawPoint(handpoint, (curTime-preTime)/Core.getTickFrequency());
//            			dt = (curTime-preTime)/Core.getTickFrequency();
//            			preTime = curTime;
//            			queue.add(cdata);
//            			queue.add(cdata);
//            			dx = cdata.getdx(pdata.p);
//            			dy = cdata.getdy(pdata.p);
//            			d =  cdata.getd(pdata.p);
//            			text_x.setText("x:"+Integer.toString((int)cdata.p.x));
//                        text_y.setText("y:"+Integer.toString((int)cdata.p.y));
//                        text_dx.setText("dx:"+Integer.toString((int)dx));
//                        text_dy.setText("dy:"+Integer.toString((int)dy));
//                        text_d.setText("d:"+Integer.toString((int)d));
//                        text_dt.setText("dt:"+Integer.toString((int)(dt*1000))+"ms");
//            		}
            		
//            		getPoint = CoordinateTransfer(handpoint);
//                    text_x.setText("x:"+Integer.toString((int)getPoint.x));
//                    text_y.setText("y:"+Integer.toString((int)getPoint.y));
                                                         
                	tHandler.obtainMessage(DRAW_CURSOR_POINT, handpoint).sendToTarget();
                	
                	keytovalue(click);
            	}
            }
        }  
    };

    /**
     * This class will receive a callback once the OpenCV library is loaded.
     */
    private static final class OpenCVLoaderCallback extends BaseLoaderCallback {
        private Context mContext;

        public OpenCVLoaderCallback(Context context) {
            super(context);
            mContext = context;
        }

        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                	
        	       try{
        	    	   System.loadLibrary("native-lib");
        	           //To do - add your static code
        	       }
        	       catch(UnsatisfiedLinkError e) {
        	            Log.v(TAG, "Native code library failed to load.\n" + e);
        	       }         
        	       catch(Exception e) {
        	            Log.v(TAG, "Exception: " + e);
        	       }
        	   
                	
                    ((SoftKeyboard) mContext).initCameraView();
                    Singleton.getInstance().m_context = mContext;
                    break;
                default:
                    super.onManagerConnected(status);
                    break;
            }
        }

    }
    
    
    private void initCameraView() {
    	if( mWorker == null){
    		mWorker = new OpenCVWorker(OpenCVWorker.FIRST_CAMERA);
    		mWorker.addResultCallback(this);
    		new Thread(mWorker).start();
    	}else{
    		mWorker.addResultCallback(this);
    		mSurfaceView.setVisibility(View.VISIBLE);
    		drawCanvas = true;
    		mWorker.drawCanvas = true;
    	}
                
    }
    
    private void setThr_Acc(String setfile){
    	File sf = new File(Environment.getExternalStorageDirectory(),setfile);
    	BufferedReader reader = null;
    	try {
    		reader = new BufferedReader(new FileReader(sf));
    		String tempString = null;
    		String para[];
			while ((tempString = reader.readLine()) != null) {
				para = tempString.split("_");
				randomacc = Integer.parseInt(para[0]);
				if (randomacc == 0) {
					numerator = Integer.parseInt(para[1]);
					denominator = Integer.parseInt(para[2]);
					threshold = Integer.parseInt(para[3]);
				}
			} 
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                }
            }
        }
    }
    
    /**
     * Main initialization of the input method component.  Be sure to call
     * to super class.
     */
    @Override
    public void onCreate() {
        super.onCreate();
        Log.e("start","testoncreate");
        Log.d("MyService", "MainActivity thread id is " + Thread.currentThread().getId());
        Log.d("MyService", "MainActivity process id is " + Process.myPid());
        Log.d("MyService", "MainActivity thread id is " + Process.myTid());
        
        Log.e("thre", Integer.toString(numerator) + ';' + Integer.toString(denominator) + ';' + Integer.toString(threshold));
        //setThr_Acc("setfile.txt");
        Log.e("thre", Integer.toString(numerator) + ';' + Integer.toString(denominator) + ';' + Integer.toString(threshold));

        Log.e("lingzhen","onCreate()");
        cmdTurnCursorServiceOn();
        mInputMethodManager = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
        mWordSeparators = getResources().getString(R.string.word_separators);
        newView = getLayoutInflater().inflate(
                R.layout.linear_layout, null);
        newLinearLayout = newView.findViewById(R.id.FrameLayout_Layout1);
        m_panel_view = newView.findViewById(R.id.FrameLayout_Layout2);
        LinearLayout_text = newView.findViewById(R.id.LinearLayout2);
        
      //SCREEN_DIM_WAKE_LOCK
      	pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
      	mWakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "My Tag");

        
//        samplePoints = new Point[SAMPLE_NUM][2];
//		for (int i = 0; i < SAMPLE_NUM; i++)
//		{
//			for (int j = 0; j < 2; j++)
//			{
//				samplePoints[i][j] = new Point();
//			}
//		}
//		
//		avgColor = new double[SAMPLE_NUM][3];
//		avgBackColor = new double[SAMPLE_NUM][3];
//		
//		//Lab
//		initCLowerUpper(50, 50, 10, 10, 10, 10);
//	    initCBackLowerUpper(50, 50, 3, 3, 3, 3);
//	    
//	    
//	    
	    mGestureDetector = new GestureDetector(new MyOnGestureListener());
        mGestureDetector.setOnDoubleTapListener(this);
        mGestureDetector.setIsLongpressEnabled(false);

        mFpsPaint = new Paint();
        mFpsPaint.setColor(Color.GREEN);
        mFpsPaint.setDither(true);
        mFpsPaint.setFlags(Paint.SUBPIXEL_TEXT_FLAG);
        mFpsPaint.setTextSize(28);
        mFpsPaint.setTypeface(Typeface.SANS_SERIF);

        DisplayMetrics dm = new DisplayMetrics();
        WindowManager winMgr = (WindowManager)getSystemService(Context.WINDOW_SERVICE);
       	winMgr.getDefaultDisplay().getMetrics(dm);
       	
       	double screenheight = dm.heightPixels*0.3;
       	double screenwidth = dm.widthPixels*0.3;
       	double mymargin = dm.widthPixels/2;
       	FrameLayout.LayoutParams layoutParams=
                new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins((int)(mymargin), 0, 0, 0); 
        layoutParams.gravity = Gravity.LEFT | Gravity.TOP;
       	mSurfaceView = new SurfaceView(this);
//        mSurfaceView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
//                ViewGroup.LayoutParams.MATCH_PARENT));
       	mSurfaceView.setLayoutParams(layoutParams);
        mSurfaceHolder = mSurfaceView.getHolder();
        mSurfaceView.getHolder().setFixedSize((int)screenwidth, (int)screenheight);
        // Create a Handler that we can post messages to so we avoid having to use anonymous Runnables
        // and runOnUiThread() instead
        mSurfaceViewHandler = new Handler(getMainLooper(), new SurfaceViewUiCallback());
        text_x = new TextView(this);
        text_y = new TextView(this);
        text_dx = new TextView(this);
        text_dy = new TextView(this);
        text_d = new TextView(this);
        text_dt = new TextView(this);
    }
    
    
    /**
     * This is the point where you can do all of your UI initialization.  It
     * is called after creation and any configuration change.
     */
    @Override
    public void onInitializeInterface() {
    	
        if (mQwertyKeyboard != null) {
            // Configuration changes can happen after the keyboard gets recreated,
            // so we need to be able to re-build the keyboards if the available
            // space has changed.
            int displayWidth = getMaxWidth();
            if (displayWidth == mLastDisplayWidth) return;
            mLastDisplayWidth = displayWidth;
        }
//        Log.e("lingzhen","lingzhen call onInitializeInterface");
        mQwertyKeyboard = new LatinKeyboard(this, R.xml.qwerty);
        mSymbolsKeyboard = new LatinKeyboard(this, R.xml.symbols);
        mSymbolsShiftedKeyboard = new LatinKeyboard(this, R.xml.symbols_shift);
        //lingzhen 
        mKeypad = new LatinKeyboard(this, R.xml.keypad);
        mQwertyKeyboard.setParameter(1);
        mRandomQwertyKeyboard = new LatinKeyboard(this, R.xml.qwerty);
        mQwertyKeyboard.setParameter(2);
        mRandomSymbolsKeyboard = new LatinKeyboard(this, R.xml.symbols);
        mQwertyKeyboard.setParameter(3);
        mRandomSymbolsShiftedKeyboard = new LatinKeyboard(this, R.xml.symbols_shift);
        //lingzhen
    }
    
    // create keys' coordinates information file
    // Sabrina 14-07-10
    public void createKeyfile(){     
    	Log.e("chenqi","createKeyfile() run.");
        DisplayMetrics dm = new DisplayMetrics();
        
        File keyinfo = new File(Environment.getExternalStorageDirectory(),"keyfile.txt");
//         File keyinfo = new File(getFilesDir(),"keyfile.txt");
//         Log.e("chenqi", getFilesDir().getAbsolutePath()+'/'+"keyfile.txt");
        Log.e("chenqi", Environment.getExternalStorageDirectory().getAbsolutePath()+'/'+"keyfile.txt");

       	
		try {
			WindowManager winMgr = (WindowManager)getSystemService(Context.WINDOW_SERVICE);
	       	winMgr.getDefaultDisplay().getMetrics(dm);
	       	
	       	int screenheight = dm.heightPixels;
	       	int screenwidth = dm.widthPixels;
	       	int keyheight = (int)(KEY_HEIGHT * dm.density + (float)0.5);// dip --> pixel
	       	int ystart = screenheight - keyheight * ROWNUM;
	       	int xstart = 0;
	        	       	
	       	FileOutputStream writer = new FileOutputStream(keyinfo);
	       	
	       	int i = 0;
	       	int len = keylista.length;
	       	Log.e("length", Integer.toString(len));
	       	double keywidth,gap;
	       	// row 1
			for (; i < 10; i++) {
				keywidth = 0.1 * (double) screenwidth;
                keyinfoa[i][0] = keylista[i];
                keyinfoa[i][1] = Integer.toString((int) (xstart + i * keywidth));
                keyinfoa[i][2] = Integer.toString(ystart);
                keyinfoa[i][3] = Integer.toString((int) (xstart + (1 + i) * keywidth));
                keyinfoa[i][4] = Integer.toString(ystart + keyheight);
	       	}
			//row 2
			for (; i < 19; i++){
				gap = 0.05 * (double) screenwidth;
				keywidth = 0.1 * (double) screenwidth;
                keyinfoa[i][0] = keylista[i];
                keyinfoa[i][1] = Integer.toString((int) (gap + xstart + (i-10) * keywidth));
                keyinfoa[i][2] = Integer.toString(ystart + keyheight);
                keyinfoa[i][3] = Integer.toString((int) (gap + xstart + (i-9) * keywidth));
                keyinfoa[i][4] = Integer.toString(ystart + 2*keyheight);
			}
			//row 3 i=19
			double[] kwa = {0.15 * (double) screenwidth,0.1 * (double) screenwidth,0.1 * (double) screenwidth,
					0.1 * (double) screenwidth,0.1 * (double) screenwidth,0.1 * (double) screenwidth,
					0.1 * (double) screenwidth,0.1 * (double) screenwidth,0.15 * (double) screenwidth};
			for (; i < 28; i++) {
				keywidth = kwa[i - 19];
                keyinfoa[i][0] = keylista[i];
                keyinfoa[i][1] = Integer.toString(xstart);
                keyinfoa[i][2] = Integer.toString(ystart + 2 * keyheight);
                keyinfoa[i][3] = Integer.toString((int) (xstart + keywidth));
                keyinfoa[i][4] = Integer.toString(ystart + 3 * keyheight);
				xstart = (int) (xstart + keywidth);
			}
			//row 4   i=28
			double[] kw = {0.2 * (double) screenwidth,0.15 * (double) screenwidth,0.3 * (double) screenwidth,
					0.15 * (double) screenwidth,0.2 * (double) screenwidth};
			xstart = 0;
			for (; i < len; i++) {
				keywidth = kw[i - 28];
                keyinfoa[i][0] = keylista[i];
                keyinfoa[i][1] = Integer.toString(xstart);
                keyinfoa[i][2] = Integer.toString(ystart + 3 * keyheight);
                keyinfoa[i][3] = Integer.toString((int) (xstart + keywidth));
                keyinfoa[i][4] = Integer.toString(ystart + 4 * keyheight);
				xstart = (int) (xstart + keywidth);
			}

            for(int j=0;j<len;j++){
                for(int m=0;m<row;m++){
                    writer.write((keyinfoa[j][m]).getBytes());
                    if(m<row-1)
                        writer.write(',');
                }
                writer.write('\n');
            }
			writer.write('\n');
			
			i=0;
			len = keylistb.length;
			xstart = 0;
			// row 1
			for (; i < 10; i++) {
				keywidth = 0.1 * (double) screenwidth;
                keyinfob[i][0] = keylistb[i];
                keyinfob[i][1] = Integer.toString((int) (xstart + i * keywidth));
                keyinfob[i][2] = Integer.toString(ystart);
                keyinfob[i][3] = Integer.toString((int) (xstart + (1 + i) * keywidth));
                keyinfob[i][4] = Integer.toString(ystart + keyheight);
			}
			// row 2
			for (; i < 19; i++) {
				gap = 0.05 * (double) screenwidth;
				keywidth = 0.1 * (double) screenwidth;
                keyinfob[i][0] = keylistb[i];
                keyinfob[i][1] = Integer.toString((int) (gap + xstart + (i - 10) * keywidth));
                keyinfob[i][2] = Integer.toString(ystart + keyheight);
                keyinfob[i][3] = Integer.toString((int) (gap + xstart + (i - 9) * keywidth));
                keyinfob[i][4] = Integer.toString(ystart + 2 * keyheight);
			}
			//row 3 i=19
			for (; i < 28; i++) {
				keywidth = kwa[i - 19];
                keyinfob[i][0] = keylistb[i];
                keyinfob[i][1] = Integer.toString(xstart);
                keyinfob[i][2] = Integer.toString(ystart + 2 * keyheight);
                keyinfob[i][3] = Integer.toString((int) (xstart + keywidth));
                keyinfob[i][4] = Integer.toString(ystart + 3 * keyheight);
				xstart = (int) (xstart + keywidth);
			}
			// row 4 i=28
			xstart = 0;
			for (; i < len; i++) {
				keywidth = kw[i - 28];
                keyinfob[i][0] = keylistb[i];
                keyinfob[i][1] = Integer.toString(xstart);
                keyinfob[i][2] = Integer.toString(ystart + 3 * keyheight);
                keyinfob[i][3] = Integer.toString((int) (xstart + keywidth));
                keyinfob[i][4] = Integer.toString(ystart + 4 * keyheight);
				xstart = (int) (xstart + keywidth);
			}

            for(int j=0;j<len;j++){
                for(int m=0;m<row;m++){
                    writer.write((keyinfob[j][m]).getBytes());
                    if(m<row-1)
                        writer.write(',');
                }
                writer.write('\n');
            }
			writer.write('\n');
			
			i = 0;
			len = keylistc.length;
			Log.e("length", Integer.toString(len));  //34
			xstart = 0;
			// row 1
			for (; i < 10; i++) {
				keywidth = 0.1 * (double) screenwidth;
                keyinfoc[i][0] = keylistc[i];
                keyinfoc[i][1] = Integer.toString((int) (xstart + i * keywidth));
                keyinfoc[i][2] = Integer.toString(ystart);
                keyinfoc[i][3] = Integer.toString((int) (xstart + (1 + i) * keywidth));
                keyinfoc[i][4] = Integer.toString(ystart + keyheight);
			}
			// row 2
			for (; i < 20; i++) {
				keywidth = 0.1 * (double) screenwidth;
                keyinfoc[i][0] = keylistc[i];
                keyinfoc[i][1] = Integer.toString((int) (xstart + (i - 10) * keywidth));
                keyinfoc[i][2] = Integer.toString(ystart + keyheight);
                keyinfoc[i][3] = Integer.toString((int) ( xstart + (i - 9) * keywidth));
                keyinfoc[i][4] = Integer.toString(ystart + 2 * keyheight);
			}
			//row 3 i=20
			for (; i < 29; i++) {
				keywidth = kwa[i - 20];
                keyinfoc[i][0] = keylistc[i];
                keyinfoc[i][1] = Integer.toString(xstart);
                keyinfoc[i][2] = Integer.toString(ystart + 2 * keyheight);
                keyinfoc[i][3] = Integer.toString((int) (xstart + keywidth));
                keyinfoc[i][4] = Integer.toString(ystart + 3 * keyheight);
				xstart = (int) (xstart + keywidth);
			}
			// row 4 i=29
			xstart = 0;
			for (; i < len; i++) {
				keywidth = kw[i - 29];
                keyinfoc[i][0] = keylistc[i];
                keyinfoc[i][1] = Integer.toString(xstart);
                keyinfoc[i][2] = Integer.toString(ystart + 3 * keyheight);
                keyinfoc[i][3] = Integer.toString((int) (xstart + keywidth));
                keyinfoc[i][4] = Integer.toString(ystart + 4 * keyheight);
				xstart = (int) (xstart + keywidth);
			}

            for(int j=0;j<len;j++){
                for(int m=0;m<row;m++){
                    writer.write((keyinfoc[j][m]).getBytes());
                    if(m<row-1)
                        writer.write(',');
                }
                writer.write('\n');
            }

			writer.write('\n');

			i = 0;
			len = keylistd.length;
			xstart = 0;
			// row 1
			for (; i < 10; i++) {
				keywidth = 0.1 * (double) screenwidth;
                keyinfod[i][0] = keylistd[i];
                keyinfod[i][1] = Integer.toString((int) (xstart + i * keywidth));
                keyinfod[i][2] = Integer.toString(ystart);
                keyinfod[i][3] = Integer.toString((int) (xstart + (1 + i) * keywidth));
                keyinfod[i][4] = Integer.toString(ystart + keyheight);
			}
			// row 2
			for (; i < 20; i++) {
				keywidth = 0.1 * (double) screenwidth;
                keyinfod[i][0] = keylistd[i];
                keyinfod[i][1] = Integer.toString((int) (xstart + (i - 10) * keywidth));
                keyinfod[i][2] = Integer.toString(ystart + keyheight);
                keyinfod[i][3] = Integer.toString((int) ( xstart + (i - 9) * keywidth));
                keyinfod[i][4] = Integer.toString(ystart + 2 * keyheight);
			}
			//row 3 i=20
			for (; i < 29; i++) {
				keywidth = kwa[i - 20];
                keyinfod[i][0] = keylistd[i];
                keyinfod[i][1] = Integer.toString(xstart);
                keyinfod[i][2] = Integer.toString(ystart + 2 * keyheight);
                keyinfod[i][3] = Integer.toString((int) (xstart + keywidth));
                keyinfod[i][4] = Integer.toString(ystart + 3 * keyheight);
				xstart = (int) (xstart + keywidth);
			}
			// row 4 i=29
			xstart = 0;
			for (; i < len; i++) {
				keywidth = kw[i - 29];
                keyinfod[i][0] = keylistd[i];
                keyinfod[i][1] = Integer.toString(xstart);
                keyinfod[i][2] = Integer.toString(ystart + 3 * keyheight);
                keyinfod[i][3] = Integer.toString((int) (xstart + keywidth));
                keyinfod[i][4] = Integer.toString(ystart + 4 * keyheight);
				xstart = (int) (xstart + keywidth);
			}

			Log.e("length", Integer.toString(len));
            for(int j=0;j<len;j++){
                for(int m=0;m<row;m++){
                    writer.write((keyinfod[j][m]).getBytes());
                    if(m<row-1)
                        writer.write(',');
                }
                writer.write('\n');
            }
            
            writer.write('\n');

			i = 0;
			len = keylistp.length;
			xstart = 0;
			// row 1
			for (; i < 3; i++) {
				keywidth = 0.33 * (double) screenwidth;
				keyinfop[i][0] = keylistp[i];
				keyinfop[i][1] = Integer.toString((int) (xstart + i * keywidth));
				keyinfop[i][2] = Integer.toString(ystart);
				keyinfop[i][3] = Integer.toString((int) (xstart + (1 + i) * keywidth));
				keyinfop[i][4] = Integer.toString(ystart + keyheight);
			}
			// row 2
			for (; i < 6; i++) {
				keywidth = 0.33 * (double) screenwidth;
				keyinfop[i][0] = keylistp[i];
				keyinfop[i][1] = Integer.toString((int) (xstart + (i - 3) * keywidth));
				keyinfop[i][2] = Integer.toString(ystart + keyheight);
				keyinfop[i][3] = Integer.toString((int) (xstart + (i - 2) * keywidth));
				keyinfop[i][4] = Integer.toString(ystart + 2 * keyheight);
			}
			// row 3 i=20
			for (; i < 9; i++) {
				keywidth = 0.33 * (double) screenwidth;
				keyinfop[i][0] = keylistp[i];
				keyinfop[i][1] = Integer.toString((int)(xstart + (i - 6) * keywidth));
				keyinfop[i][2] = Integer.toString(ystart + 2 * keyheight);
				keyinfop[i][3] = Integer.toString((int)(xstart + (i - 5) * keywidth));
				keyinfop[i][4] = Integer.toString(ystart + 3 * keyheight);
			}
			// row 4 i=29
			for (; i < len; i++) {
				keywidth = 0.33 * (double) screenwidth;
				keyinfop[i][0] = keylistp[i];
				keyinfop[i][1] = Integer.toString((int)(xstart + (i - 9) * keywidth));
				keyinfop[i][2] = Integer.toString(ystart + 3 * keyheight);
				keyinfop[i][3] = Integer.toString((int)(xstart + (i - 8) * keywidth));
				keyinfop[i][4] = Integer.toString(ystart + 4 * keyheight);
			}

			Log.e("length", Integer.toString(len));
			for (int j = 0; j < len; j++) {
				for (int m = 0; m < row; m++) {
					writer.write((keyinfop[j][m]).getBytes());
					if (m < row - 1)
						writer.write(',');
				}
				writer.write('\n');
			}
			
			writer.flush(); 
		    writer.close();  
		}
		catch (IOException e){
			Log.e("length","IOException");
			e.printStackTrace();
		}
		catch (Exception e) { //default to a HVGA 320x480 and let's hope for the best
			Log.e("length","Exception");
			e.printStackTrace();
		}    
		
		Log.e("length","createKeyfile() over.");
  	}
    
    ////////////////////////////////////
    //Sabrina    2014-7-18
    public void createKeyMap(){
    	keymap.put("q",113);
    	keymap.put("w",119);
    	keymap.put("e",101);
    	keymap.put("r",114);
    	keymap.put("t",116);
    	keymap.put("y",121);
    	keymap.put("u",117);
    	keymap.put("i",105);
    	keymap.put("o",111);
    	keymap.put("p",112);
    	keymap.put("a",97);
    	keymap.put("s",115);
    	keymap.put("d",100);
    	keymap.put("f",102);
    	keymap.put("g",103);
    	keymap.put("h",104);
    	keymap.put("j",106);
    	keymap.put("k",107);
    	keymap.put("l",108);
    	keymap.put("shift",-1);
    	keymap.put("z",122);
    	keymap.put("x",120);
    	keymap.put("c",99);
    	keymap.put("v",118);
    	keymap.put("b",98);
    	keymap.put("n",110);
    	keymap.put("m",109);
    	keymap.put("del",-5);
    	keymap.put("done",-3);
    	keymap.put("123",-2);
    	keymap.put("space",32);
    	keymap.put("point",46);
    	keymap.put("next",10);
    	
    	keymap.put("0", 48);
    	keymap.put("1", 49);
    	keymap.put("2", 50);
    	keymap.put("3", 51);
    	keymap.put("4", 52);
    	keymap.put("5", 53);
    	keymap.put("6", 54);
    	keymap.put("7", 55);
    	keymap.put("8", 56);
    	keymap.put("9", 57);    	
    	keymap.put("@", 64);
    	keymap.put("#", 35);
    	keymap.put("$", 36);
    	keymap.put("%", 37);
    	keymap.put("&", 38);
    	keymap.put("*", 42);
    	keymap.put("-", 45);
    	keymap.put("=", 61);
    	keymap.put("(", 40);
    	keymap.put(")", 41);
    	keymap.put("!", 33);
    	keymap.put("\"", 34);
    	keymap.put("'", 39);
    	keymap.put(":", 58);
    	keymap.put(";", 59);
    	keymap.put("/", 47);
    	keymap.put("?", 63);  
    	keymap.put("ABC",-2);
    	
//    	"~","±","×","÷","•","°","`","´","{","}","©","£","€","^","®","¥","_","+","[","]","shift","¡","&lt;","&gt;","¢","|","\\","¿",
//		"del","done","ABC","space","apostrophe","next"
    	keymap.put("~", 126);
    	keymap.put("±", 177);
    	keymap.put("×", 215);
    	keymap.put("÷", 247);
    	keymap.put("•", 8226);
    	keymap.put("°", 176);
    	keymap.put("`", 96);
    	keymap.put("´", 180);
    	keymap.put("{", 123);
    	keymap.put("}", 125);    	
    	keymap.put("©", 169);
    	keymap.put("£", 163);
    	keymap.put("€", 8364);
    	keymap.put("^", 94);
    	keymap.put("®", 174);
    	keymap.put("¥", 165);
    	keymap.put("_", 95);
    	keymap.put("+", 43);
    	keymap.put("[", 91);
    	keymap.put("]", 93);
    	keymap.put("¡", 161);
    	keymap.put("&lt;", 60);
    	keymap.put("&gt;", 62);
    	keymap.put("¢", 162);
    	keymap.put("|", 124);
    	keymap.put("\\", 92);
    	keymap.put("¿", 191); 
    	
    	keymap.put("comma",44);
    	keymap.put("apostrophe", 8230);
    }
    
    /**
     * Called by the framework when your view for creating input needs to
     * be generated.  This will be called the first time your input method
     * is displayed, and every time it needs to be re-created such as due to
     * a configuration change.
     */
    @Override
    public View onCreateInputView() {
    	Log.e("lingzhen","onCreateInputView");
    	
    	    	
        mInputView = (LatinKeyboardView) getLayoutInflater().inflate(
                R.layout.input, null);
        mInputView.setOnKeyboardActionListener(this);
        mInputView.setKeyboard(mQwertyKeyboard);

		// ///////////////////////
		createKeyfile();
		createKeyMap();
		// ////////////////////////
		
		
//		(getWindow()).addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//		mWakeLock.acquire(); 

//        mOpenCvView = getLayoutInflater().inflate(
//                R.layout.face_detect_surface_view, null);    
//        setInputView(mOpenCvView);
        
//        mOpenCvCameraView = (CameraBridgeViewBase) mOpenCvView.findViewById(R.id.fd_activity_surface_view);
//        mOpenCvCameraView = (MyCameraView) mOpenCvView.findViewById(R.id.HandGestureApp);
//        mOpenCvCameraView.setCameraIndex(0); //rear camera
//        mOpenCvCameraView.setCameraIndex(1); //front camera
//        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
//        mOpenCvCameraView.setCvCameraViewListener(this);
//        FrameLayout newView = new FrameLayout(this);
        
        
        try{
        	drawCanvas = false;
//        	mWorker.drawCanvas = false;
//        	newLinearLayout.setOrientation(Configuration.ORIENTATION_LANDSCAPE);
        	Log.e("lingzhen", "Hello0");
        	newLinearLayout.removeAllViews();
//        	m_panel_view.removeAllViews();
        	Log.e("lingzhen", "Hello");
        	newLinearLayout.addView(mInputView);
        	Log.e("lingzhen", "Hello1");
//        	newLinearLayout.addView(mOpenCvCameraView);
//        	m_panel_view.addView(mOpenCvCameraView);
        	m_panel_view.addView(mSurfaceView);
        	LinearLayout_text.addView(text_x);
        	LinearLayout_text.addView(text_y);
        	LinearLayout_text.addView(text_dx);
        	LinearLayout_text.addView(text_dy);
        	LinearLayout_text.addView(text_d);
        	LinearLayout_text.addView(text_dt);
//        	m_panel_view.addView(LinearLayout_text);
        	
        	Log.e("lingzhen", "Hello2");
        	drawCanvas = true;
//        	mWorker.drawCanvas = true;
//        	newLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
        	
        }catch(Exception e){
        	Log.e("lingzhen", ""+e.getMessage());
        	e.printStackTrace();
        }
        return newView;
//        return mInputView;
    }
    
    //lingzhen.2014.05.10
    @Override
    public void requestHideSelf(int flags) {
        super.requestHideSelf(flags);
        Log.e("lingzhen","requestHideSelf:"+flags);
        if(flags == 0){
        	cmdHideCursor();
            m_bRunning = false;
            mWorker.drawCanvas = false;

            drawCanvas = false;
        }
    }
    
    private void cmdTurnCursorServiceOn() {	
       	Intent i = new Intent();
        i.setAction("com.example.qqq.cmake.CursorService");
//    	Intent i = new Intent(this, CursorService.class);
        startService(i);
	}
	
	private void cmdTurnCursorServiceOff() {
		Intent i = new Intent();
        i.setAction("com.example.qqq.cmake.CursorService");
        stopService(i);
//        Intent i = new Intent(this, CursorService.class);
//        stopService(i);
	}
    
	private void cmdShowCursor() {
		if (Singleton.getInstance().m_CurService != null)
			Singleton.getInstance().m_CurService.ShowCursor(true);
	}
	
	private void cmdHideCursor() {
		if (Singleton.getInstance().m_CurService != null)
			Singleton.getInstance().m_CurService.ShowCursor(false);
	}
    
    /**
     * Called by the framework when your view for showing candidates needs to
     * be generated, like {@link #onCreateInputView}.
     */
    @Override
    public View onCreateCandidatesView() {
        mCandidateView = new CandidateView(this);
        mCandidateView.setService(this);
        return mCandidateView;
    }

    /**
     * This is the main point where we do our initialization of the input method
     * to begin operating on an application.  At this point we have been
     * bound to the client, and are now receiving all of the detailed information
     * about the target of our edits.
     */
    @Override
    public void onStartInput(EditorInfo attribute, boolean restarting) {
        super.onStartInput(attribute, restarting);

        // Reset our state.  We want to do this even if restarting, because
        // the underlying state of the text editor could have changed in any way.
        mComposing.setLength(0);
        updateCandidates();
        setCandidatesViewShown(false);
//        setCandidatesViewShown(true);

        if (!restarting) {
            // Clear shift states.
            mMetaState = 0;
        }

        mPredictionOn = false;
        mCompletionOn = false;
//        mCompletionOn = true;
        mCompletions = null;

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
//        boolean random_flag = prefs.getBoolean("random_setting", true);
        boolean random_flag = false;


        // We are now going to initialize our state based on the type of
        // text being edited.
        switch (attribute.inputType & InputType.TYPE_MASK_CLASS) {
            ///lingzhen
//	    	case InputType.TYPE_NUMBER_VARIATION_PASSWORD:
//	    	case InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD:
//	    	case InputType.TYPE_TEXT_VARIATION_WEB_PASSWORD:
//	    		Log.e("lingzhen","lingzhen Hi Passwword");
//	    		mPredictionOn = false;
//	    		mQwertyKeyboard.setParameter(1);
//	    		mCurKeyboard = new LatinKeyboard(this, R.xml.qwerty);
//	    		break;
            case InputType.TYPE_CLASS_NUMBER:
            case InputType.TYPE_CLASS_DATETIME:
                // Numbers and dates default to the symbols keyboard, with
                // no extra features.
                //lingzhen
                mCurKeyboard = mSymbolsKeyboard;
//            	mCurKeyboard = mKeypad;
                int variation = attribute.inputType & InputType.TYPE_MASK_VARIATION;
//            	Log.e("lingzhen","lingzhen variation:"+variation);
//            	Log.e("lingzhen","lingzhen attribute.inputType:"+attribute.inputType);
//            	Log.e("lingzhen","lingzhen InputType.TYPE_CLASS_NUMBER:"+InputType.TYPE_CLASS_NUMBER);
//            	Log.e("lingzhen","lingzhen InputType.TYPE_NUMBER_VARIATION_PASSWORD:"+InputType.TYPE_NUMBER_VARIATION_PASSWORD);
                if (variation == InputType.TYPE_NUMBER_VARIATION_PASSWORD ||
                        variation == InputType.TYPE_TEXT_VARIATION_PASSWORD ||
                        variation == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD ||
                        variation == InputType.TYPE_TEXT_VARIATION_WEB_PASSWORD) {
                    // Do not display predictions / what the user is typing
                    // when they are entering a password.
                    mPredictionOn = false;
//                    mCurKeyboard = mKeypad;
                    mCurKeyboard = mSymbolsKeyboard;
                    //lingzhen
                    if (random_flag) {
                        mRandomSymbolsKeyboard.setParameter(2);
                        mRandomSymbolsKeyboard = new LatinKeyboard(this, R.xml.symbols);
                        mCurKeyboard = mRandomSymbolsKeyboard;
                        mRandomQwertyKeyboard.setParameter(1);
                        mRandomQwertyKeyboard = new LatinKeyboard(this, R.xml.qwerty);
                        mRandomSymbolsShiftedKeyboard.setParameter(3);
                        mRandomSymbolsShiftedKeyboard = new LatinKeyboard(this, R.xml.symbols_shift);
                    }
                }
                //lingzhen
//                mCurKeyboard = mSymbolsKeyboard;
                break;

            case InputType.TYPE_CLASS_PHONE:
                // Phones will also default to the symbols keyboard, though
                // often you will want to have a dedicated phone keyboard.
//                mCurKeyboard = mSymbolsKeyboard;
                mCurKeyboard = mKeypad;
                break;

            case InputType.TYPE_CLASS_TEXT:
                // This is general text editing.  We will default to the
                // normal alphabetic keyboard, and assume that we should
                // be doing predictive text (showing candidates as the
                // user types).
                mCurKeyboard = mQwertyKeyboard;
                mPredictionOn = true;

                // We now look for a few special variations of text that will
                // modify our behavior.
                variation = attribute.inputType & InputType.TYPE_MASK_VARIATION;
                if (variation == InputType.TYPE_TEXT_VARIATION_PASSWORD ||
                        variation == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD ||
                        variation == InputType.TYPE_TEXT_VARIATION_WEB_PASSWORD) {
                    // Do not display predictions / what the user is typing
                    // when they are entering a password.
                    mPredictionOn = false;
//                    mCurKeyboard = mKeypad;
                    mCurKeyboard = mSymbolsKeyboard;
                    //lingzhen
//                    Log.e("lingzhen","lingzhen Hi Passwword");
                    if (random_flag) {
                        mRandomQwertyKeyboard.setParameter(1);
                        mRandomQwertyKeyboard = new LatinKeyboard(this, R.xml.qwerty);
                        mCurKeyboard = mRandomQwertyKeyboard;
                        mRandomSymbolsKeyboard.setParameter(2);
                        mRandomSymbolsKeyboard = new LatinKeyboard(this, R.xml.symbols);
                        mRandomSymbolsShiftedKeyboard.setParameter(3);
                        mRandomSymbolsShiftedKeyboard = new LatinKeyboard(this, R.xml.symbols_shift);
                    }
                    //lingzhen                    
                }

                if (variation == InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
                        || variation == InputType.TYPE_TEXT_VARIATION_URI
                        || variation == InputType.TYPE_TEXT_VARIATION_FILTER) {
                    // Our predictions are not useful for e-mail addresses
                    // or URIs.
                    mPredictionOn = false;
                }

                if ((attribute.inputType & InputType.TYPE_TEXT_FLAG_AUTO_COMPLETE) != 0) {
                    // If this is an auto-complete text view, then our predictions
                    // will not be shown and instead we will allow the editor
                    // to supply their own.  We only show the editor's
                    // candidates when in fullscreen mode, otherwise relying
                    // own it displaying its own UI.
                    mPredictionOn = false;
                    mCompletionOn = isFullscreenMode();
                }

                // We also want to look at the current state of the editor
                // to decide whether our alphabetic keyboard should start out
                // shifted.
                updateShiftKeyState(attribute);
                break;

            default:
                // For all unknown input types, default to the alphabetic
                // keyboard with no special features.
                mCurKeyboard = mQwertyKeyboard;
                updateShiftKeyState(attribute);
        }

        // Update the label on the enter key, depending on what the application
        // says it will do.
        //lingzhen
        //attribute.imeOptions |= EditorInfo.IME_FLAG_NO_EXTRACT_UI;
        attribute.imeOptions |= EditorInfo.IME_FLAG_NO_FULLSCREEN;
        mCurKeyboard.setImeOptions(getResources(), attribute.imeOptions);
        //Log.e("lingzhen","lingzhen onStartInput End");

    }

    /**
     * This is called when the user is done editing a field.  We can use
     * this to reset our state.
     */
    @Override
    public void onFinishInput() {
        super.onFinishInput();
        cmdHideCursor();
//        m_bRunning = false;
		// close our cursor service
//		cmdTurnCursorServiceOff();
        Log.e("lingzhen", "onFinishInput()");
        if (mWorker != null)
        {
            mWorker.mIsEnabled = false;
            mWorker.removeResultCallback(this);
        }

        // Clear current composing text and candidates.
        mComposing.setLength(0);
        updateCandidates();
        
        // We only hide the candidates window when finishing input on
        // a particular editor, to avoid popping the underlying application
        // up and down if the user is entering text into the bottom of
        // its window.
        setCandidatesViewShown(false);
        if (mWakeLock != null){
        	Log.v("lingzhen", "Releasing mWakeLock");
        	try {
            	mWakeLock.release();          		
        	}catch (Throwable th) {
        		
        	}
        } else {
        	Log.e("lingzhen", "mWakeLock reference is null");
        }

        
        mCurKeyboard = mQwertyKeyboard;
        if (mInputView != null) {
            mInputView.closing();
        }
    }
    
    @Override
    public void onDestroy() {
    	super.onDestroy();
    	Log.e("lingzhen", "onDestroy()");
        cmdHideCursor();
//		m_bRunning = false;
		// close our cursor service
		cmdTurnCursorServiceOff();
		mWorker.drawCanvas = false;
		drawCanvas = false;
		mSurfaceView.setVisibility(SurfaceView.GONE);
		mWorker.stopProcessing();
        mWorker.removeResultCallback(this);
    }
    
    @Override
    public void onStartInputView(EditorInfo attribute, boolean restarting) {
        super.onStartInputView(attribute, restarting);
        if(inital == 0){
        	SystemClock.sleep(350);
        }

        if (mWorker != null)
            mWorker.mIsEnabled = true;
        
        mWakeLock.acquire(); 
        
		// ///////////////////////
		// 2014-07-11 Sabrina

		inputstate = true;

//		EditorInfo ei = getCurrentInputEditorInfo();
//		if (ei.actionId == EditorInfo.IME_ACTION_DONE) {
//			inputstate = false;
//			try {
//				FileOutputStream fs = new FileOutputStream(rawcontrolinfo, true);
//				Log.e("abc", "end of input!");
//				fs.write(("\n\n").getBytes());
//				fs.close();
//				
//				FileOutputStream fos = new FileOutputStream(rawdisplayinfo, true);
//				Log.e("abc", "end of input!");
//				fos.write(("\n\n").getBytes());
//				fos.close();
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}

		Log.e("StartInputView", "onStartInputView()");
        
     // get screen size
        DisplayMetrics metrics = new DisplayMetrics();
		try {
			WindowManager winMgr = (WindowManager)getSystemService(Context.WINDOW_SERVICE);
	       	winMgr.getDefaultDisplay().getMetrics(metrics);
	       	m_nScreenW = winMgr.getDefaultDisplay().getWidth();
	       	m_nScreenH = winMgr.getDefaultDisplay().getHeight();
	       	Log.e("lingzhen", Integer.toString(m_nScreenW)+" "+ Integer.toString(m_nScreenH)
	       			+" "+ Integer.toString(metrics.heightPixels)+" "+ Integer.toString(metrics.widthPixels));
	       	Log.e("lingzhen", Integer.toString(DisplayMetrics.DENSITY_DEFAULT)+" "+ Float.toString(metrics.densityDpi)+" "+ Float.toString(metrics.ydpi));
		}
		catch (Exception e) { //default to a HVGA 320x480 and let's hope for the best
			e.printStackTrace();
			m_nScreenW = 0;
			m_nScreenH = 0;
		} 
        
        Log.e("lingzhen", "onStartInputView: m_nScreenW:"+m_nScreenW+" m_nScreenW:"+m_nScreenH);
        
        //start SurfaceView
        mSurfaceHolder.addCallback(this);
        mSurfaceView.setOnTouchListener(this);                 
        
        // Apply the selected keyboard to the input view.
        mInputView.setKeyboard(mCurKeyboard);
        Log.e("lingzhen123", "Hello "+ Integer.toString(mInputView.getWidth())+" "+ Integer.toString(mInputView.getHeight()));
        mInputView.closing();
        final InputMethodSubtype subtype = mInputMethodManager.getCurrentInputMethodSubtype();
        mInputView.setSubtypeOnSpaceKey(subtype);
        
        
//        mOpenCvView = (View) getLayoutInflater().inflate(
//                R.layout.face_detect_surface_view, null);    
//        setInputView(mOpenCvView);
//        mOpenCvCameraView = (CameraBridgeViewBase) mOpenCvView.findViewById(R.id.fd_activity_surface_view);
//        mOpenCvCameraView.setCvCameraViewListener(this);
        //lingzhen11
//        if(OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this, mLoaderCallback)){
        if(OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION, this, new OpenCVLoaderCallback(this))){
        	Log.i(TAG, "Loaded OpenCV");
        }else{
        	Log.i(TAG, "Couldn't load OpenCV");
        }
        
//        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
//        boolean random_flag = prefs.getBoolean("random_setting", true);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        int acc_index = prefs.getInt("Acceleration", 10);
        int thre_index = prefs.getInt("Sensitivy", 3);
        
        if(Singleton.getInstance().m_CurService != null){
	        cmdShowCursor();
	        m_bRunning = true;
			// start a thread to move cursor arround
	        if(inital == 0){
	        	inital = inital + 1;
	        	Thread t = new Thread() {
	        		
	        		int now_x = 0, now_y = 0;
	        		int last_x = 0, last_y = 0;
	        		int cursor_x = 0, cursor_y = 0;
	        		int dx = 0, dy = 0, px = 0, py = 0;
	        		float xremainder = 0, yremainder = 0;
	        		int ini = 0;
	        		
	        		public void run(){
	        			Looper.prepare();
		        		Log.d("MyService", "Accelerating thread id is " + Thread.currentThread().getId());
		        		Log.d("MyService", "Accelerating process id is " + Process.myPid());
		        		Log.d("MyService", "Accelerating thread id is " + Process.myTid());
	        			tHandler = new Handler() {
	                        public void handleMessage(Message msg) {
	                        	
//	                        	curx = (int)getPoint.x;
//	                        	cury = (int)getPoint.y;
	                        	
	                        	if (msg.what == DRAW_CURSOR_POINT) {
                                    
	                                Point resultMouseDelta = (Point) msg.obj;
	                                
//	                                now_x = (int) Math.ceil(resultMousePoint.x);
//									now_y = (int) Math.ceil(resultMousePoint.y);
//									Log.e("chqiq","handpoint:"+","+Double.toString(now_x)+","+Double.toString(now_y));
									
									long t;
									
									if (ini != 0) {

			                    		t = Core.getTickCount();
			                    		
			                    		if (randomacc == 1){
			                    			Random rand = new Random();
			                				numerator = acc[rand.nextInt(6)];
			                				denominator = 2;
			                				threshold = thre[rand.nextInt(6)];
			                    		}
//			                    		Log.e("cfws", Integer.toString((int)randomacc)+","+Integer.toString((int)numerator)+","+Integer.toString((int)threshold));
			                    		
//										dx = now_x - last_x;
//										dy = now_y - last_y;
										dx = resultMouseDelta.x;
										dy = resultMouseDelta.y;
										if (threshold != 0) {
											if ((Math.abs(dx) + Math.abs(dy)) >= threshold) {
												xremainder = ((float) dx * (float) numerator) / (float) (denominator) + xremainder;
												px = (int) xremainder;
												xremainder = xremainder - (float) (px);

												yremainder = ((float) dy * (float) numerator) / (float) (denominator) + yremainder;
												py = (int) yremainder;
												yremainder = yremainder - (float) (py);

												cursor_x = cursor_x + px;
												cursor_y = cursor_y + py;
											} 
											else {
												cursor_x = cursor_x + dx;
												cursor_y = cursor_y + dy;
											}
										}

									} else {

										init = Core.getTickCount();
										t = init;
										
										ini = ini + 1;
//										cursor_x = now_x;
//										cursor_y = now_y;
										//getPoint = new Point(m_nScreenW/2,m_nScreenH - 0.5*(double)mInputView.getHeight());
                                        getPoint = new Point(m_nScreenW/2,m_nScreenH - mInputView.getHeight()/2);
										cursor_x = getPoint.x;
										cursor_y = getPoint.y;
									}			
									
//									if(cursor_x >= mWorker.getPreviewSize().width-1)
//		                    			cursor_x = (int)mWorker.getPreviewSize().width-1;
//		                    		else if(cursor_x <= 0)
//		                    			cursor_x = 0;
//		                    		
//		                    		if(cursor_y >= mWorker.getPreviewSize().height-1)
//		                    			cursor_y = (int)mWorker.getPreviewSize().height-1;
//		                    		else if(cursor_y <= 0)
//		                    			cursor_y = 0;

//		                			getPoint = CoordinateTransfer(new Point(cursor_x,cursor_y));
		                    		
		                    		//limit the scope of the cursor

		                    		if(cursor_x >= m_nScreenW-20)
		                    			cursor_x = m_nScreenW-20;
		                    		else if(cursor_x <= 0)
		                    			cursor_x = 0;
		                    		
		                    		if(cursor_y >= m_nScreenH-20)
		                    			cursor_y = m_nScreenH-20;
		                    		else if(cursor_y <= (m_nScreenH-mInputView.getHeight()))
		                    			cursor_y = m_nScreenH-mInputView.getHeight()-1;

		                    		getPoint = new Point(cursor_x,cursor_y);
		                    		
//		                    		Log.e("chqiq","curpoint:"+","+Double.toString(cursor_x)+","+Double.toString(cursor_y));
		                    		Log.e("sumocf", Integer.toString(m_nScreenW)+","+ Integer.toString(m_nScreenH)+","+ Integer.toString(m_nScreenH-mInputView.getHeight()));
		                    		Log.e("sumo", Integer.toString(cursor_x)+","+ Integer.toString(cursor_y));
		                    		Log.e("sumo", Integer.toString(getPoint.x)+","+ Integer.toString(getPoint.y));
		                    				                			
		                    		Singleton.getInstance().m_CurService.Update(getPoint.x, getPoint.y, true);
		                    		
//		                			try {
//		                				FileOutputStream fos = new FileOutputStream(xyinfo, true);
//		                				fos.write(("("+Integer.toString((int)getPoint.x)+","+Integer.toString((int)getPoint.y)+","+ Double.toString((double)(t-init)/Core.getTickFrequency())+")"+"\n").getBytes());
//		                				fos.close();
//		                				
//										if (cury != 0) {
//											FileOutputStream fod = new FileOutputStream(dxyinfo, true);
//											fod.write(("(" + Integer .toString((int) getPoint.x - curx)+","+Integer.toString((int)getPoint.y-cury)+","+ Double.toString((double)(t-init)/Core.getTickFrequency())+")"+"\n").getBytes());
//											fod.close();
//										}
//		                			} catch (Exception e) {
//		                				e.printStackTrace();
//		                			}
		                    				                    		
									try {
										Thread.sleep(5);
									} catch (InterruptedException e) {
										e.printStackTrace();
									}
									
//									last_x = now_x;
//									last_y = now_y;
	                        	}
	                        	
	                        }
	        			};
	        	        Looper.loop();
	        		}
	        	};
	        	t.start();
	        }
        }
    }

    @Override
    public void onCurrentInputMethodSubtypeChanged(InputMethodSubtype subtype) {
        mInputView.setSubtypeOnSpaceKey(subtype);
        Log.i("lingzhen", "onCurrentInputMethodSubtypeChanged()");
    }

    /**
     * Deal with the editor reporting movement of its cursor.
     */
    @Override
    public void onUpdateSelection(int oldSelStart, int oldSelEnd,
                                  int newSelStart, int newSelEnd,
                                  int candidatesStart, int candidatesEnd) {
//    	Log.e("lingzhen","lingzhen");
    	//Log.e("lingzhen","lingzhen B:"+oldSelStart+" "+oldSelEnd+" "+newSelStart
        //		+" "+newSelEnd+" "+candidatesStart+" "+candidatesEnd);
        super.onUpdateSelection(oldSelStart, oldSelEnd, newSelStart, newSelEnd,
                candidatesStart, candidatesEnd);
        //Log.e("lingzhen","lingzhen A:"+oldSelStart+" "+oldSelEnd+" "+newSelStart
        //		+" "+newSelEnd+" "+candidatesStart+" "+candidatesEnd);
        // If the current selection in the text view changes, we should
        // clear whatever candidate text we have.
        if (mComposing.length() > 0 && (newSelStart != candidatesEnd
                || newSelEnd != candidatesEnd)) {
            mComposing.setLength(0);
            updateCandidates();
            Log.e("lingzhen","lingzhen onUpdateSelection");
            InputConnection ic = getCurrentInputConnection();
            if (ic != null) {
                ic.finishComposingText();
            }
        }
    }
    
    //lingzhen start
    @Override
    public void onExtractedCursorMovement(int dx, int dy) {
    	//Log.e("lingzhen","lingzhen B: dx:"+dx+" dy:"+dy);
        super.onExtractedCursorMovement(dx, dy);
        //Log.e("lingzhen","lingzhen A: dx:"+dx+" dy:"+dy);
    }
    
//    @Override public void onExtractedSetSpan(Object span, int start, int end, int flags){
//    	super.onExtractedSetSpan(span, start, end, flags);    	
//    }
    @Override
    public void onExtractedSelectionChanged(int start, int end) {
    	//Log.e("lingzhen","lingzhen B: start:"+start+" end:"+end);
        super.onExtractedSelectionChanged(start, end);
        //Log.e("lingzhen","lingzhen A: start:"+start+" end:"+end);
    }
    @Override
    public void onUpdateExtractedText(int token, ExtractedText text) {
    	//Log.e("lingzhen","lingzhen onUpdateExtractedText start:"+text.selectionStart
    	//		+" end:"+text.selectionEnd+"token:"+token);
    	text.selectionStart = text.selectionEnd;
    	super.onUpdateExtractedText(token, text);
    }
    
    @Override
    public void onUpdateExtractingViews(EditorInfo ei) {
    	//Log.e("lingzhen","lingzhen onUpdateExtractingViews");
    	super.onUpdateExtractingViews(ei);
    }
    
    @Override
    public void onExtractedTextClicked() {
    	//Log.e("lingzhen","lingzhen onExtractedTextClicked");
    	super.onExtractedTextClicked();
    }
    
    @Override
    public void onExtractingInputChanged(EditorInfo ei) {
    	//Log.e("lingzhen","lingzhen onExtractingInputChanged");
    	super.onExtractingInputChanged(ei);
    }
    //lingzhen end
    
    /**
     * This tells us about completions that the editor has determined based
     * on the current text in it.  We want to use this in fullscreen mode
     * to show the completions ourself, since the editor can not be seen
     * in that situation.
     */
    @Override
    public void onDisplayCompletions(CompletionInfo[] completions) {
    	//Log.e("lingzhen","lingzhen onDisplayCompletions");
        if (mCompletionOn) {
            mCompletions = completions;
            if (completions == null) {
                setSuggestions(null, false, false);
                return;
            }
            
            List<String> stringList = new ArrayList<String>();
            for (int i = 0; i < completions.length; i++) {
                CompletionInfo ci = completions[i];
                if (ci != null) stringList.add(ci.getText().toString());
            }
            setSuggestions(stringList, true, true);
        }
    }
    
    /**
     * This translates incoming hard key events in to edit operations on an
     * InputConnection.  It is only needed when using the
     * PROCESS_HARD_KEYS option.
     */
    private boolean translateKeyDown(int keyCode, KeyEvent event) {
    	Log.e("chq","translateKeyDown() run.");
        mMetaState = MetaKeyKeyListener.handleKeyDown(mMetaState,
                keyCode, event);
        int c = event.getUnicodeChar(MetaKeyKeyListener.getMetaState(mMetaState));
        mMetaState = MetaKeyKeyListener.adjustMetaAfterKeypress(mMetaState);
        InputConnection ic = getCurrentInputConnection();
        if (c == 0 || ic == null) {
            return false;
        }
        
        boolean dead = false;

        if ((c & KeyCharacterMap.COMBINING_ACCENT) != 0) {
            dead = true;
            c = c & KeyCharacterMap.COMBINING_ACCENT_MASK;
        }
        
        if (mComposing.length() > 0) {
            char accent = mComposing.charAt(mComposing.length() -1 );
            int composed = KeyEvent.getDeadChar(accent, c);

            if (composed != 0) {
                c = composed;
                mComposing.setLength(mComposing.length()-1);
            }
        }
        
        onKey(c, null);
        
        return true;
    }
    
    /**
     * Use this to monitor key events being delivered to the application.
     * We get first crack at them, and can either resume them or let them
     * continue to the app.
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
    	Log.e("chq","onKeyDown() run.");
        switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:
			// /////////////////////////
			// 2014-07-11 Sabrina
			inputstate = false;
//			try {
//				FileOutputStream fos = new FileOutputStream(rawdisplayinfo, true);
//				Log.e("abc", "end of input!");
//				fos.write(("\n").getBytes());
//				fos.close();
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
			// The InputMethodService already takes care of the back
			// key for us, to dismiss the input method if it is shown.
			// However, our keyboard could be showing a pop-up window
			// that back should dismiss, so we first allow it to do that.
			if (event.getRepeatCount() == 0 && mInputView != null) {
				if (mInputView.handleBack()) {
					// lingzhen.2014.05.10
					Log.e("lingzhen", "onKeyDown KeyEvent.KEYCODE_BACK");
					cmdHideCursor();
					return true;
				}
			}
			break;

		case KeyEvent.KEYCODE_DEL:
			// Special handling of the delete key: if we currently are
			// composing text for the user, we want to modify that instead
			// of let the application to the delete itself.
			if (mComposing.length() > 0) {
				onKey(Keyboard.KEYCODE_DELETE, null);
				return true;
			}
			break;

		case KeyEvent.KEYCODE_ENTER:
			Log.e("cfws","KeyEvent.KEYCODE_ENTER go!" + Integer.toString(keyCode));
			// Let the underlying text editor always handle these.
			return false;

		default:
			// For all other keys, if we want to do transformations on
			// text being entered with a hard keyboard, we need to process
			// it and do the appropriate action.
			if (PROCESS_HARD_KEYS) {
				if (keyCode == KeyEvent.KEYCODE_SPACE
						&& (event.getMetaState() & KeyEvent.META_ALT_ON) != 0) {
					// A silly example: in our input method, Alt+Space
					// is a shortcut for 'android' in lower case.
					InputConnection ic = getCurrentInputConnection();
					if (ic != null) {
						// First, tell the editor that it is no longer in the
						// shift state, since we are consuming this.
						ic.clearMetaKeyStates(KeyEvent.META_ALT_ON);
						keyDownUp(KeyEvent.KEYCODE_A);
						keyDownUp(KeyEvent.KEYCODE_N);
						keyDownUp(KeyEvent.KEYCODE_D);
						keyDownUp(KeyEvent.KEYCODE_R);
						keyDownUp(KeyEvent.KEYCODE_O);
						keyDownUp(KeyEvent.KEYCODE_I);
						keyDownUp(KeyEvent.KEYCODE_D);
						// And we consume this event.
						return true;
					}
				}
				if (mPredictionOn && translateKeyDown(keyCode, event)) {
					return true;
				}
			}
		}

        return super.onKeyDown(keyCode, event);
    }

    /**
     * Use this to monitor key events being delivered to the application.
     * We get first crack at them, and can either resume them or let them
     * continue to the app.
     */
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        // If we want to do transformations on text being entered with a hard
        // keyboard, we need to process the up events to update the meta key
        // state we are tracking.
    	Log.e("chq","onKeyUp() run.");
        if (PROCESS_HARD_KEYS) {
            if (mPredictionOn) {
                mMetaState = MetaKeyKeyListener.handleKeyUp(mMetaState,
                        keyCode, event);
            }
        }
        
        return super.onKeyUp(keyCode, event);
    }

    /**
     * Helper function to commit any text being composed in to the editor.
     */
    private void commitTyped(InputConnection inputConnection) {
        if (mComposing.length() > 0) {
//            lingzhen inputConnection.commitText(mComposing, mComposing.length());
        	inputConnection.commitText(mComposing, 1);
            mComposing.setLength(0);
            updateCandidates();
        }
    }

    /**
     * Helper to update the shift state of our keyboard based on the initial
     * editor state.
     */
    private void updateShiftKeyState(EditorInfo attr) {
        if (attr != null 
                && mInputView != null && mQwertyKeyboard == mInputView.getKeyboard()) {
            int caps = 0;
            EditorInfo ei = getCurrentInputEditorInfo();
            if (ei != null && ei.inputType != InputType.TYPE_NULL) {
                caps = getCurrentInputConnection().getCursorCapsMode(attr.inputType);
            }
            mInputView.setShifted(mCapsLock || caps != 0);
        }
    }
    
    /**
     * Helper to determine if a given character code is alphabetic.
     */
    private boolean isAlphabet(int code) {
        return Character.isLetter(code);
    }
    
    /**
     * Helper to send a key down / key up pair to the current editor.
     */
    private void keyDownUp(int keyEventCode) {
    	Log.e("cfws","keyDownUp() run.");
        getCurrentInputConnection().sendKeyEvent(
                new KeyEvent(KeyEvent.ACTION_DOWN, keyEventCode));
        getCurrentInputConnection().sendKeyEvent(
                new KeyEvent(KeyEvent.ACTION_UP, keyEventCode));
    }
    
    /**
     * Helper to send a character to the editor as raw key events.
     */
    private void sendKey(int keyCode) {
    	Log.e("chq","sendKey() run.");
        switch (keyCode) {
            case '\n':
            	Log.e("cfws","sendKey '\\n' go!" + Integer.toString(keyCode));
                keyDownUp(KeyEvent.KEYCODE_ENTER);
                break;
            default:
                if (keyCode >= '0' && keyCode <= '9') {
                    keyDownUp(keyCode - '0' + KeyEvent.KEYCODE_0);
                } else {
                    getCurrentInputConnection().commitText(String.valueOf((char) keyCode), 1);
                }
                break;
        }
    }

    //////////////////////////////////
    //Sabrina 2013-07-18
    public String transferkeycoordinate(String[][] keyinfo, int n, Point lastpoint){
    	Log.e("chqiqi", Double.toString(lastpoint.x)+","+ Double.toString(lastpoint.y));
    	if(lastpoint.x < 0 || lastpoint.y < (m_nScreenH - mInputView.getHeight()) || lastpoint.x > m_nScreenW || lastpoint.y > m_nScreenH )
    		return null;
    	for(int i=0; i<n; i++)
    	{
    		if (lastpoint.x > Integer.parseInt(keyinfo[i][1]) && lastpoint.x < Integer.parseInt(keyinfo[i][3])
    				&& lastpoint.y > Integer.parseInt(keyinfo[i][2]) && lastpoint.y < Integer.parseInt(keyinfo[i][4])){
    			Log.e("chqiq", Integer.parseInt(keyinfo[i][1])+","+ Integer.parseInt(keyinfo[i][3])+","+ Integer.parseInt(keyinfo[i][2])+","+ Integer.parseInt(keyinfo[i][4]));
    			Log.e("chqiq",keyinfo[i][0]);
    			return keyinfo[i][0];
    		}
    	}
        return null;
    }
    
    public int getKeycode(String s){
    	if(s == null)
    		return KeyEvent.KEYCODE_UNKNOWN;
    	return keymap.get(s);
    }

    // Implementation of KeyboardViewListener

	public void onKey(int primaryCode, int[] keyCodes) {
		Log.e("chq", "onKey() run.");
		Log.e("chq", Integer.toString(primaryCode));
		Log.e("chqi", String.valueOf((char) primaryCode));
		// Log.e("lingzhen","lingzhen onKey primaryCode"+primaryCode+"keyCodes"+keyCodes[0]);
		//
		// for(int i = 0; i < keyCodes.length; i++)
		// Log.e("chqi",Integer.toString(keyCodes[i]));
		//
		
		if (isWordSeparator(primaryCode)) {
			// Handle separator
			if (mComposing.length() > 0) {
				commitTyped(getCurrentInputConnection());
			}
			sendKey(primaryCode);
			int f = (getCurrentInputEditorInfo().inputType & InputType.TYPE_MASK_VARIATION);
			Log.e("cfws","sendKey go!" + Integer.toString(f));
			
			updateShiftKeyState(getCurrentInputEditorInfo());
		} else if (primaryCode == Keyboard.KEYCODE_DELETE) {
			handleBackspace();
		} else if (primaryCode == Keyboard.KEYCODE_SHIFT) {
			handleShift();
		} else if (primaryCode == Keyboard.KEYCODE_CANCEL) {
			handleClose();
			return;
		} else if (primaryCode == LatinKeyboardView.KEYCODE_OPTIONS) {
			// Show a menu or somethin'
		} else if (primaryCode == Keyboard.KEYCODE_MODE_CHANGE
				&& mInputView != null) {
			// lingzhen
			EditorInfo ei = getCurrentInputEditorInfo();
			Keyboard current = mInputView.getKeyboard();
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
//			boolean random_flag = prefs.getBoolean("random_setting", true);
			boolean random_flag = false;
			int variation = ei.inputType & InputType.TYPE_MASK_VARIATION;
			if (variation == InputType.TYPE_TEXT_VARIATION_PASSWORD
					|| variation == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
					|| variation == InputType.TYPE_TEXT_VARIATION_WEB_PASSWORD) {
				// Do not display predictions / what the user is typing
				// when they are entering a password.
				mPredictionOn = false;
				// lingzhen
				// Log.e("lingzhen","lingzhen Hi Passwword");
				// mQwertyKeyboard.setParameter(1);
				// mCurKeyboard = new LatinKeyboard(this, R.xml.qwerty);
				if (random_flag) {
					if (current == mRandomSymbolsKeyboard
							|| current == mRandomSymbolsShiftedKeyboard) {
						current = mRandomQwertyKeyboard;
					} else {

						current = mRandomSymbolsKeyboard;
					}
				} else {
					if (current == mSymbolsKeyboard
							|| current == mSymbolsShiftedKeyboard) {
						current = mQwertyKeyboard;
					} else {
						current = mSymbolsKeyboard;
					}
				}
				mInputView.setKeyboard(current);
				if (current == mRandomSymbolsKeyboard) {
					current.setShifted(false);
				}
				// lingzhen
			} else {
				if (current == mSymbolsKeyboard
						|| current == mSymbolsShiftedKeyboard) {
					current = mQwertyKeyboard;
				} else {
					current = mSymbolsKeyboard;
				}
				mInputView.setKeyboard(current);
				if (current == mSymbolsKeyboard) {
					current.setShifted(false);
				}
			}
			
		} else if (primaryCode == LatinKeyboard.KEYCODE_RND) { // lingzhen
			// Log.e("lingzhen","lingzhen primaryCode"+primaryCode+"ifif");
			Keyboard current = mInputView.getKeyboard();
			EditorInfo ei = getCurrentInputEditorInfo();
			Keyboard current1 = mInputView.getKeyboard();
			int variation = ei.inputType & InputType.TYPE_MASK_VARIATION;
			if (variation == InputType.TYPE_TEXT_VARIATION_PASSWORD
					|| variation == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
					|| variation == InputType.TYPE_TEXT_VARIATION_WEB_PASSWORD) {
				mPredictionOn = false;
				mQwertyKeyboard.setParameter(1);
				mRandomQwertyKeyboard = new LatinKeyboard(this, R.xml.qwerty);
				current1 = mRandomQwertyKeyboard;
				mRandomSymbolsKeyboard.setParameter(2);
				mRandomSymbolsKeyboard = new LatinKeyboard(this, R.xml.symbols);
				mRandomSymbolsShiftedKeyboard.setParameter(3);
				mRandomSymbolsShiftedKeyboard = new LatinKeyboard(this, R.xml.symbols_shift);
			}
			
			mInputView.setKeyboard(current1);
		} else {
			// Log.e("lingzhen","lingzhen primaryCode"+primaryCode+"final"+keyCodes[0]);
			Log.e("cfws","handleCharacter go!" + Integer.toString(primaryCode));
			handleCharacter(primaryCode, keyCodes);
		}
	}

    public void onText(CharSequence text) {
        InputConnection ic = getCurrentInputConnection();
        if (ic == null) return;
        ic.beginBatchEdit();
        if (mComposing.length() > 0) {
            commitTyped(ic);
        }
        ic.commitText(text, 0);
        ic.endBatchEdit();
        updateShiftKeyState(getCurrentInputEditorInfo());
    }

    /**
     * Update the list of available candidates from the current composing
     * text.  This will need to be filled in by however you are determining
     * candidates.
     */
    private void updateCandidates() {
        if (!mCompletionOn) {
            if (mComposing.length() > 0) {
                ArrayList<String> list = new ArrayList<String>();
                list.add(mComposing.toString());
                setSuggestions(list, true, true);
            } else {
                setSuggestions(null, false, false);
            }
        }
    }
    
    public void setSuggestions(List<String> suggestions, boolean completions,
                               boolean typedWordValid) {
        if (suggestions != null && suggestions.size() > 0) {
            setCandidatesViewShown(true);
        } else if (isExtractViewShown()) {
            setCandidatesViewShown(true);
        }
        if (mCandidateView != null) {
            mCandidateView.setSuggestions(suggestions, completions, typedWordValid);
        }
    }
    
    private void handleBackspace() {
    	//Log.e("lingzhen", "lingzhen handleBackspace");
        final int length = mComposing.length();
        if (length > 1) {
            mComposing.delete(length - 1, length);
            getCurrentInputConnection().setComposingText(mComposing, 1);
            updateCandidates();
        } else if (length > 0) {
            mComposing.setLength(0);
            getCurrentInputConnection().commitText("", 0);
            updateCandidates();
        } else {
            keyDownUp(KeyEvent.KEYCODE_DEL);
        }
        updateShiftKeyState(getCurrentInputEditorInfo());
    }

    private void handleShift() {
        if (mInputView == null) {
            return;
        }
        Keyboard currentKeyboard = mInputView.getKeyboard();
        if (mQwertyKeyboard == currentKeyboard) {
            // Alphabet keyboard
            checkToggleCapsLock();
            mInputView.setShifted(mCapsLock || !mInputView.isShifted());
        } else if (currentKeyboard == mSymbolsKeyboard) {
            mSymbolsKeyboard.setShifted(true);
            mInputView.setKeyboard(mSymbolsShiftedKeyboard);
            mSymbolsShiftedKeyboard.setShifted(true);
        } else if (currentKeyboard == mSymbolsShiftedKeyboard) {
            mSymbolsShiftedKeyboard.setShifted(false);
            mInputView.setKeyboard(mSymbolsKeyboard);
            mSymbolsKeyboard.setShifted(false);
        } else if (mRandomQwertyKeyboard == currentKeyboard){ //lingzhen
        	checkToggleCapsLock();
            mInputView.setShifted(mCapsLock || !mInputView.isShifted());
        } else if (currentKeyboard == mRandomSymbolsKeyboard){ //lingzhen
        	mRandomSymbolsKeyboard.setShifted(true);
//        	mRandomSymbolsShiftedKeyboard.setParameter(3);
//        	mRandomSymbolsShiftedKeyboard = new LatinKeyboard(this, R.xml.symbols_shift);
            mInputView.setKeyboard(mRandomSymbolsShiftedKeyboard);
            mRandomSymbolsShiftedKeyboard.setShifted(true);
        } else if (currentKeyboard == mRandomSymbolsShiftedKeyboard) { //lingzhen
            mRandomSymbolsShiftedKeyboard.setShifted(false);
            mInputView.setKeyboard(mRandomSymbolsKeyboard);
            mRandomSymbolsKeyboard.setShifted(false);
        }
    }
    
    private void handleCharacter(int primaryCode, int[] keyCodes) {
    	Log.e("chq","handleCharacter() run.");
        if (isInputViewShown()) {
            if (mInputView.isShifted()) {
                primaryCode = Character.toUpperCase(primaryCode);
            }
        }
        if (isAlphabet(primaryCode) && mPredictionOn) {
            mComposing.append((char) primaryCode);
            getCurrentInputConnection().setComposingText(mComposing, 1);
            updateShiftKeyState(getCurrentInputEditorInfo());
            updateCandidates();
        } else {
        	Log.e("cfws","handleCharacter else go!" + Integer.toString(primaryCode));
        	if(mComposing.length() > 0){ //lingzhen
        		mComposing.append((char) primaryCode);
        		getCurrentInputConnection().setComposingText(mComposing, 1);
        	}else{ //lingzhen
        		getCurrentInputConnection().commitText(
                    String.valueOf((char) primaryCode), 1);
        	}
        }
    }

    private void handleClose() {
        commitTyped(getCurrentInputConnection());
        requestHideSelf(0);
        mInputView.closing();
    }

    private void checkToggleCapsLock() {
        long now = System.currentTimeMillis();
        if (mLastShiftTime + 800 > now) {
            mCapsLock = !mCapsLock;
            mLastShiftTime = 0;
        } else {
            mLastShiftTime = now;
        }
    }
    
    private String getWordSeparators() {
        return mWordSeparators;
    }
    
    public boolean isWordSeparator(int code) {
        String separators = getWordSeparators();
        return separators.contains(String.valueOf((char)code));
    }

    public void pickDefaultCandidate() {
        pickSuggestionManually(0);
    }
    
    public void pickSuggestionManually(int index) {
        if (mCompletionOn && mCompletions != null && index >= 0
                && index < mCompletions.length) {
            CompletionInfo ci = mCompletions[index];
            getCurrentInputConnection().commitCompletion(ci);
            if (mCandidateView != null) {
                mCandidateView.clear();
            }
            updateShiftKeyState(getCurrentInputEditorInfo());
        } else if (mComposing.length() > 0) {
            // If we were generating candidate suggestions for the current
            // text, we would commit one of them here.  But for this sample,
            // we will just commit the current text.
            commitTyped(getCurrentInputConnection());
        }
    }
    
    public void swipeRight() {
        if (mCompletionOn) {
            pickDefaultCandidate();
        }
    }
    
    public void swipeLeft() {
        handleBackspace();
    }

    public void swipeDown() {
        handleClose();
    }

    public void swipeUp() {
    }
    
    public void onPress(int primaryCode) {
    }
    
    public void onRelease(int primaryCode) {
    }

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e) {
		// TODO Auto-generated method stub
        /*int mode = mWorker.mode;
        String toastStr = null;
        if (mode == mWorker.BACKGROUND_MODE) {
        	toastStr = "First background sampled!";
        	mode = mWorker.SAMPLE_MODE;
        }else if (mode == mWorker.SAMPLE_MODE) {
        	mode = mWorker.DETECTION_MODE;
        	toastStr = "Sampling Finished!";
        } else if (mode == mWorker.DETECTION_MODE) {
        	mode = mWorker.TRAIN_REC_MODE;
        	toastStr = "Binary Display Finished!";
        }   	    	    	    	            
    	Toast.makeText(getApplicationContext(), toastStr, Toast.LENGTH_LONG).show();
    	mWorker.mode = mode;*/
    	
        return true;
	}

	@Override
	public boolean onDoubleTap(MotionEvent e) {
		// TODO Auto-generated method stub
		mWorker.drawCanvas = false;
		drawCanvas = false;
		mSurfaceView.setVisibility(View.GONE);
		return true;
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		return mGestureDetector.onTouchEvent(event);
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
//		mWorker.drawCanvas = true;
//		drawCanvas = true;
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
		// TODO Auto-generated method stub
		mSurfaceSize = new RectF(0, 0, width, height);
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		mWorker.drawCanvas = false;
		drawCanvas = false;
	}

	@Override
	public void onResultMatrixReady(Bitmap resultBitmap) {
		// TODO Auto-generated method stub		
		if(drawCanvas == true){
			mSurfaceViewHandler.obtainMessage(DRAW_RESULT_BITMAP, resultBitmap).sendToTarget();
		}
	}
	
	public void keytovalue(int click){

		Keyboard currentKeyboard = mInputView.getKeyboard();
		mCurKeyboard = (LatinKeyboard) currentKeyboard;

		InputConnection ic = getCurrentInputConnection();
		String key;
		if (ic != null) {
			if (click == 1) {
				int len = 0;
				if (currentKeyboard == mQwertyKeyboard) {
					subkeyinfo = keyinfoa;
					len = keylista.length;
				} else if (currentKeyboard == mSymbolsKeyboard) {
					subkeyinfo = keyinfoc;
					len = keylistc.length;
				} else if (currentKeyboard == mSymbolsShiftedKeyboard) {
					subkeyinfo = keyinfod;
					len = keylistd.length;
				}else if (currentKeyboard == mKeypad) {
					subkeyinfo = keyinfop;
					len = keylistp.length;
				}
				
				key = transferkeycoordinate(subkeyinfo, len, getPoint);
//				Log.e("chqiqi",
//						"lastpoint:" + "," + Double.toString(curx)
//								+ "," + Double.toString(cury));
				onKey(getKeycode(key), null);
				
			}
		}
			
	}
	
	Point CoordinateTransfer(Point OrignialPoint){
		Point TransferPoint = new Point();
		Size previewSize = mWorker.getPreviewSize();
		double xFactor = 1.2 * (double)mInputView.getWidth() / previewSize.width;
		double yFactor = 1.2 * (double)mInputView.getHeight() / previewSize.height;
		double widthoffset = 0.1 * (double)mInputView.getWidth();
		double heightoffset = (double)m_nScreenH - 1.1*(double)mInputView.getHeight();
//        Log.e("sumo","screen getWidth:"+Integer.toString(m_nScreenW)+" getHeight"+Integer.toString(m_nScreenH));
        Log.w("sumo","preview getWidth:"+ Double.toString(previewSize.width)+" getHeight"+ Double.toString(previewSize.height));
        Log.w("sumo","mInputView getWidth:"+ Integer.toString(mInputView.getWidth())+"getHeight"+ Integer.toString(mInputView.getHeight()));
        Log.w("sumo","xFactor:"+ Double.toString(xFactor)+" yFactor"+ Double.toString(yFactor));
        //TransferPoint.x = (double)OrignialPoint.x * xFactor - (double)widthoffset ;
        //ResultCallbackTransferPoint.y = (double)OrignialPoint.y * yFactor + (double)heightoffset ;
        TransferPoint.x = OrignialPoint.x *(int) xFactor - (int)widthoffset ;
        TransferPoint.y = OrignialPoint.y * (int)yFactor + (int)heightoffset ;
        return TransferPoint;
	}


//	@Override
//	public  void onMousePoint(Point handpoint, int click, long currenttime) {
//
//		// TODO Auto-generated method stub
//		if(mHandler != null){
//			double result[] = {handpoint.x,handpoint.y,click};
//			mHandler.obtainMessage(RAW_POINT, result).sendToTarget();
////			Log.e("lingzhen1:", "receive a message from opencvworker:" + Integer.toString((int)Math.ceil(handpoint.x))+"y:"+Integer.toString((int)Math.ceil(handpoint.y)));
////			Log.e("lzw", "Send a message to the child thread - x:" + Integer.toString((int)Math.ceil(getPoint.x))+"y:"+Integer.toString((int)Math.ceil(getPoint.y)));
//
//		}
//	}
	
	@Override
	public void onFpsUpdate(double fps) {
		// TODO Auto-generated method stub
		mFpsResult = fps;
	}

    @Override
    public void onMousePoint(org.opencv.core.Point handpoint, int click, long currenttime) {
//         TODO Auto-generated method stub
		if(mHandler != null){
			double result[] = {handpoint.x,handpoint.y,click};
			mHandler.obtainMessage(RAW_POINT, result).sendToTarget();
//			Log.e("lingzhen1:", "receive a message from opencvworker:" + Integer.toString((int)Math.ceil(handpoint.x))+"y:"+Integer.toString((int)Math.ceil(handpoint.y)));
//			Log.e("lzw", "Send a message to the child thread - x:" + Integer.toString((int)Math.ceil(getPoint.x))+"y:"+Integer.toString((int)Math.ceil(getPoint.y)));

		}
    }

    /**
     * This Handler callback is used to draw a bitmap to our SurfaceView.
     */
    private class SurfaceViewUiCallback implements Handler.Callback {
        @Override
        public boolean handleMessage(Message message) {
            if (message.what == DRAW_RESULT_BITMAP) {
                Bitmap resultBitmap = (Bitmap) message.obj;
                Canvas canvas = null;
                try {
                    canvas = mSurfaceHolder.lockCanvas();
                    
//                    Matrix rotator = new Matrix();
//                    rotator.postRotate(90);
////                    rotator.postRotate(-90);   //这个是用于角度翻转转的
//////                  rotator.preScale(1,-1); //front camera
////                    rotator.preScale(-1,-1); //rear camera 这个是用于对称翻转
//                    Bitmap rotatedBitmap = Bitmap.createBitmap(resultBitmap , 0, 0, resultBitmap.getWidth(), resultBitmap.getHeight(), rotator, true);
//                    Log.e("BitmapSize:",Integer.toString(resultBitmap.getWidth())+','+Integer.toString(resultBitmap.getHeight()));
//                    Log.e("afBitmapSize:",Integer.toString(rotatedBitmap.getWidth())+','+Integer.toString(rotatedBitmap.getHeight()));
//                    
//                    canvas.drawBitmap(rotatedBitmap, null, mSurfaceSize, null);
                    canvas.drawBitmap(resultBitmap, null, mSurfaceSize, null);
//                    Log.e("surfaceSize:",Double.toString(mSurfaceSize.width())+','+Double.toString(mSurfaceSize.height()));
                    canvas.drawText(String.format("FPS: %.2f", mFpsResult), 35, 45, mFpsPaint);
                    String msg = "Double-tap to clear SurfaceView.";
                    float width = mFpsPaint.measureText(msg);
//                    canvas.drawText(msg, mSurfaceView.getWidth() / 2 - width / 2, mSurfaceView.getHeight() - 30, mFpsPaint);
                }catch (Exception e) { //default to a HVGA 320x480 and let's hope for the best
        			Log.e("chqiq","Exception");
        			e.printStackTrace();
                } finally {
                    if (canvas != null) {
                        mSurfaceHolder.unlockCanvasAndPost(canvas);
                    }
                    // Tell the worker that the bitmap is ready to be reused
                    mWorker.releaseResultBitmap(resultBitmap);
                }
            }
            return true;
        }
    }
	
    private class MyOnGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onDown(MotionEvent event) {
            return true;
        }
    }
}
